window.Router = Backbone.Router.extend({

        routes: {
            "": "home",
            "products": "products",
            "company": "company",
            "contact": "contact"
        },

        initialize: function () {
            this.sidebarView = new SidebarView();
            $('#wrapper #sidebar').html(this.sidebarView.render().el);
            this.home();
        },

        home : function() {
            this.loadView(new HomeView());
        },

        products : function() {
            this.loadView(new ProductsView());
        },

        company : function() {
            this.loadView(new CompanyView());
        },

        contact : function() {
            this.loadView(new ContactView());
        },

        loadView : function(view) {
            this.view && this.view.remove();
            this.view = view.render();
            $("main article").html(view.el);
            this.sidebarView.select(view.id);
        }

    });

    templateLoader.load(["HomeView", "SidebarView","ProductsView", "CompanyView", "ContactView"],
    function () {
        app = new Router();
        Backbone.history.start();
});