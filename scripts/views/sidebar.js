window.SidebarView = Backbone.View.extend({

    events:{
        "click #menu-collapse": "showNavigation"
    },

    render: function () {
        $(this.el).html(this.template());
        return this;
    },

    select: function(menuItem) {
        $('.nav li').removeClass('active');
        $('#' + menuItem).addClass('active');
    },

    showNavigation: function(){
        if($('nav').css('display') == 'none')
            $('nav').attr('style','display:block !important');
        else
            $('nav').attr('style','display:none !important');
    }
});