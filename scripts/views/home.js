window.HomeView = Backbone.View.extend({
    id: "home",
    render:function () {
        $(this.el).html(this.template());
        return this;
    }
});
