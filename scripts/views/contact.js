window.ContactView = Backbone.View.extend({
    id: "contact",
    render:function () {
        $(this.el).html(this.template());
        return this;
    }
});
