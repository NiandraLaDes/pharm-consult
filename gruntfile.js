//Gruntfile
module.exports = function(grunt) {

    //Initializing the configuration object
    grunt.initConfig({

        // Task configuration
        less: {
            development: {
                options: {
                    compress: true
                },
                files: {
                    "./styles/css/styles.css":"./styles/less/base.less"
                }
            }
        },
        watch:{
            less: {
                files: ['styles/less/*.less'],
                tasks: ['less'],
                options: {
                    livereload: true
                }
            }
        },
	web_server: {
	    options: {
	      cors: true,
	      port: 8000,
	      nevercache: true,
	      logRequests: true
	    },
	    foo: 'bar'
	},
       
    });

    // Load Plugins
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-web-server');

    // Default Task
    grunt.registerTask('default', ['watch']);


};
