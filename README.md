# Project Title

Pharm Consult Assignment

## Built With

* [Backbone.js](http://backbonejs.org/) - The javascript library used
* [Bootstrap](https://getbootstrap.com/) - Twitter Bootstrap
* [LESS](http://lesscss.org/) - LESS


## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details